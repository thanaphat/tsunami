import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormInputWorkOrderComponent } from './form-input-work-order.component';

describe('FormInputWorkOrderComponent', () => {
  let component: FormInputWorkOrderComponent;
  let fixture: ComponentFixture<FormInputWorkOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormInputWorkOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormInputWorkOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
