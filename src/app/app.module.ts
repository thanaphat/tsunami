import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

/*
  Core Library etc. Material, Bootstrap
*/
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule
, MatCheckboxModule
, MatInputModule
, MatTableDataSource
, MatIconRegistry
, MatGridListModule
} from '@angular/material';
//import { MatGridListModule } from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';

/*
  Manual Library
*/
import { NgxFitTextModule } from 'ngx-fit-text';	//  [ngxFitText]


/*
  Component
*/
import { AppComponent } from './app.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { Screen0Component } from './screen0/screen0.component';
import { Screen11Component } from './screen11/screen11.component';
import { Screen10Component } from './screen10/screen10.component';
import { HeaderScreen0Component } from './header-screen0/header-screen0.component';
import { BodyScreen0Component } from './body-screen0/body-screen0.component';
import { FormInputWorkOrderComponent } from './form-input-work-order/form-input-work-order.component';


@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    Screen0Component,
    Screen11Component,
    Screen10Component,
    HeaderScreen0Component,
    BodyScreen0Component,
    FormInputWorkOrderComponent
  ],
  imports: [
    BrowserModule,
	HttpModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatGridListModule,
    MatTableModule,
    NgxFitTextModule.forRoot(),
    NgbModule.forRoot(),
	FlexLayoutModule,
	MatFormFieldModule,
	MatInputModule,
	MatIconModule,
	MatDividerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }