import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyScreen0Component } from './body-screen0.component';

describe('BodyScreen0Component', () => {
  let component: BodyScreen0Component;
  let fixture: ComponentFixture<BodyScreen0Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyScreen0Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyScreen0Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
