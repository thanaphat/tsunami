import { Component, OnInit, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-body-screen0',
  templateUrl: './body-screen0.component.html',
  styleUrls: ['./body-screen0.component.css']
})

export class BodyScreen0Component implements OnInit {
	displayedColumns = ['position', 'item', 'time', 'qty'];
	dataSource = new MatTableDataSource(ELEMENT_DATA);

	applyFilter(filterValue: string) {
		filterValue = filterValue.trim(); // Remove whitespace
		filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
		this.dataSource.filter = filterValue;
	}

	ngOnInit(){

	}
}

export interface Element {
	position: number;
	i_item_cd: string;
	i_trade_item_cd: string;
	wo_no: string;
	process: string;
	std_start_time: string;
	std_end_time: string;
	act_start_time: string;
	act_end_time: string;
	plan_qty: number;
	act_qty: number;
}

const ELEMENT_DATA: Element[] = [
	{position: 0, i_item_cd: 'Mark', i_trade_item_cd: 'mark', wo_no: '12345667'
	, process: 'VER -> LAMVER -> LAMVER -> LAMVER -> LAMVER -> LAM', std_start_time: '08:30'
	, std_end_time: '09:00', act_start_time: '25:45', act_end_time: '26:15', plan_qty: 15000, act_qty: 135},
	{position: 1, i_item_cd: 'abcdefghigklmnopqrstuvwxyz123', i_trade_item_cd: '52421-0D010-00', wo_no: '1701039023'
	, process: 'VER -> LAMVER -> LAMVER -> LAMVER -> LAMVER -> LAM', std_start_time: '08:30'
	, std_end_time: '09:00', act_start_time: '25:45', act_end_time: '26:15', plan_qty: 15000, act_qty: 135},
	{position: 2, i_item_cd: 'AM370ND-13120170', i_trade_item_cd: '55734-02310-00', wo_no: '1701039023'
	, process: 'VER -> LAM', std_start_time: '08:50', std_end_time: '09:15', act_start_time: '08:45'
	, act_end_time: '09:15', plan_qty: 350, act_qty: 340},
	{position: 3, i_item_cd: 'AM370ND-13120170', i_trade_item_cd: '55734-02310-00', wo_no: '1701039023'
	, process: 'VER -> LAM', std_start_time: '08:50', std_end_time: '09:15', act_start_time: '08:45'
	, act_end_time: '09:15', plan_qty: 350, act_qty: 340},
	{position: 4, i_item_cd: 'AM370ND-13120170', i_trade_item_cd: '55734-02310-00', wo_no: '1701039023'
	, process: 'VER -> LAM', std_start_time: '08:50', std_end_time: '09:15', act_start_time: '08:45'
	, act_end_time: '09:15', plan_qty: 350, act_qty: 340},
	{position: 5, i_item_cd: 'AM370ND-13120170', i_trade_item_cd: '55734-02310-00', wo_no: '1701039023'
	, process: 'VER -> LAM', std_start_time: '08:50', std_end_time: '09:15', act_start_time: '08:45'
	, act_end_time: '09:15', plan_qty: 350, act_qty: 340},
	{position: 6, i_item_cd: 'AM370ND-13120170', i_trade_item_cd: '55734-02310-00', wo_no: '1701039023'
	, process: 'VER -> LAM', std_start_time: '08:50', std_end_time: '09:15', act_start_time: '08:45'
	, act_end_time: '09:15', plan_qty: 350, act_qty: 340},
	{position: 7, i_item_cd: 'AM370ND-13120170', i_trade_item_cd: '55734-02310-00', wo_no: '1701039023'
	, process: 'VER -> LAM', std_start_time: '08:50', std_end_time: '09:15', act_start_time: '08:45'
	, act_end_time: '09:15', plan_qty: 350, act_qty: 340},
	{position: 8, i_item_cd: 'AM370ND-13120170', i_trade_item_cd: '55734-02310-00', wo_no: '1701039023'
	, process: 'VER -> LAM', std_start_time: '08:50', std_end_time: '09:15', act_start_time: '08:45'
	, act_end_time: '09:15', plan_qty: 350, act_qty: 340},
	{position: 9, i_item_cd: 'AM370ND-13120170', i_trade_item_cd: '55734-02310-00', wo_no: '1701039023'
	, process: 'VER -> LAM', std_start_time: '08:50', std_end_time: '09:15', act_start_time: '08:45'
	, act_end_time: '09:15', plan_qty: 350, act_qty: 340},
	{position: 10, i_item_cd: 'AM370ND-13120170', i_trade_item_cd: '55734-02310-00', wo_no: '1701039023'
	, process: 'VER -> LAM', std_start_time: '08:50', std_end_time: '09:15', act_start_time: '08:45'
	, act_end_time: '09:15', plan_qty: 350, act_qty: 340},
	{position: 11, i_item_cd: 'AM370ND-13120170', i_trade_item_cd: '55734-02310-00', wo_no: '1701039023'
	, process: 'VER -> LAM', std_start_time: '08:50', std_end_time: '09:15', act_start_time: '08:45'
	, act_end_time: '09:15', plan_qty: 350, act_qty: 340},
	{position: 12, i_item_cd: 'AM370ND-13120170', i_trade_item_cd: '55734-02310-00', wo_no: '1701039023'
	, process: 'VER -> LAM', std_start_time: '08:50', std_end_time: '09:15', act_start_time: '08:45'
	, act_end_time: '09:15', plan_qty: 350, act_qty: 340},
	{position: 13, i_item_cd: 'AM370ND-13120170', i_trade_item_cd: '55734-02310-00', wo_no: '1701039023'
	, process: 'VER -> LAM', std_start_time: '08:50', std_end_time: '09:15', act_start_time: '08:45'
	, act_end_time: '09:15', plan_qty: 350, act_qty: 340},
	{position: 14, i_item_cd: 'AM370ND-13120170', i_trade_item_cd: '55734-02310-00', wo_no: '1701039023'
	, process: 'VER -> LAM', std_start_time: '08:50', std_end_time: '09:15', act_start_time: '08:45'
	, act_end_time: '09:15', plan_qty: 350, act_qty: 340},
	{position: 15, i_item_cd: 'AM370ND-13120170', i_trade_item_cd: '55734-02310-00', wo_no: '1701039023'
	, process: 'VER -> LAM', std_start_time: '08:50', std_end_time: '09:15', act_start_time: '08:45'
	, act_end_time: '09:15', plan_qty: 350, act_qty: 340},
];