import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderScreen0Component } from './header-screen0.component';

describe('HeaderScreen0Component', () => {
  let component: HeaderScreen0Component;
  let fixture: ComponentFixture<HeaderScreen0Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderScreen0Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderScreen0Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
