export class WorkOrder {
	wo_no:		string;
	i_item_cd:	string;
	i_item_desc:	string;
	i_trade_item_cd:	string;
	i_trade_item_desc:	string;
	i_dl_cd:	string;
	i_dl_dsec:	string;
	plan_qty:	number;
	fg_qty:		number;
	ng_qty:		number;
	ng_reason:	string;
	current_process:	string;
	next_process:	string;
	
	constructor(values: Object = {}){
		Object.assign(this, values);
	}
}
